# API Collection

Sometimes you are just out of ideas and asking yourself what to program and you just don't know where to start.
What is a original idea? Maybe you need something to practice on?
No worry, we got your back!

In this page you can find various public APIs which you can experiement and have fun with.
All new entry suggestions and improvements are welcome and if you're interested in contributing, please checkout the [wiki page](https://gitlab.com/dcacademy/tutorials/-/wikis/Submitting-a-new-entry-to-the-API-collection) about that. 😃

It's also worth linking [**APIs.io**](https://apis.io/) which is a really nice website which also gathers a bunch of APIs in a nice search engine.

## List


| Website        | [https://deckofcardsapi.com](https://deckofcardsapi.com) |
| :------------- | :------------------------------------------------------- |
| Description    | An API to simulate a deck of cards.                      |
| Authentication | None                                                     |


---


| Website        | [http://numbersapi.com](http://numbersapi.com) |
| :------------- | :--------------------------------------------- |
| Description    | An API for interesting facts about numbers.    |
| Authentication | None                                           |

---

| Website        | [https://memegen.link](https://memegen.link)                                    |
| :------------- | :------------------------------------------------------------------------------ |
| Description    | An API to programatically generate memes with an easy to understand URL format. |
| Authentication | None                                                                            |

---

| Website        | [http://goqr.me/api](http://goqr.me/api) |
| :------------- | :--------------------------------------- |
| Description    | Quick an easy QR code generation API.    |
| Authentication | None                                     |

---

| Website        | [https://www.mediawiki.org/wiki/API:Main_page](https://www.mediawiki.org/wiki/API:Main_page) |
| :------------- | :------------------------------------------------------------------------------------------- |
| Description    | The MediaWiki API allows access to some wiki-features like authentication, page operations, and search.<br>It's advisable to use one of the already existing wrappers for your favourite language, as the API is rather convoluted. |
| Authentication | None (search) / Key (modifying)                                                              |

---

| Website        | [https://developer.oxforddictionaries.com](https://developer.oxforddictionaries.com) |
| :------------- | :----------------------------------------------------------------------------------- |
| Description    | An Oxford API to access their dictionaries. The API has both paid and free tiers.    |
| Authentication | Key via signup                                                                       |

---

| Website        | [sandipbgt.com/theastrologer-api](sandipbgt.com/theastrologer-api) |
| :------------- | :----------------------------------------------------------------- |
| Description    | An un-official REST API that exposes GET endpoints to see your horoscopes from [theastrologer.com](theastrologer.com) website.<br>An instance can be found [here](https://theastrologer-api.herokuapp.com/). |
| Authentication | Key via account signup                                             |

---

| Website        | [https://dog.ceo/dog-api](https://dog.ceo/dog-api)            |
| :------------- | :------------------------------------------------------------ |
| Description    | The internet's biggest collectionof open source dog pictures. |
| Authentication | None                                                          |

---

| Website        | [https://thecatapi.com](https://thecatapi.com) |
| :------------- | :--------------------------------------------- |
| Description    | Cats as a Service, Everyday is Caturday.       |
| Authentication | Key via email                                  |

---

| Website        | [https://no-api-key.com](https://no-api-key.com) |
| :------------- | :---------------------------------------------   |
| Description    | A site where an api key isn't needed.            |
| Authentication | None                                             |


## Missing your favourite?

Contribute to the [source repository](https://gitlab.com/dcacademy/tutorials) and get your favourte APIs listed here!

