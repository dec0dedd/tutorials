# Discord API Wrappers

As a part of our collection of tutorials, we have some covering various libraries that make interacting with the Discord API actually bearable.  

Here's a list of the currently available tutorials and the ones that are still in the process of being made:
- [discord.py](./dpy/introduction.html)
- [~~Hikari~~](./hikari/introduction.html) *(eventually)*
- [~~JDA~~](./jda/introduction.html)
- [~~Discord4J~~](./d4j/introduction.html)
- [~~Javacord~~](./javacord/introduction.html)
- [~~discord.js~~](./djs/introduction.html)
- [~~cheesecord.js~~](./cjs/introduction.html) *(maybe)*

