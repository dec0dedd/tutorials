# Adding commands

So, you've learned how to process the incoming events from the gateway, but now you wonder how you can make commands to allow users to interact with your bot more easily. To do that, we'll use the `discord.ext.commands` submodule (which we were already using to create the `Bot` instance).

Adding a command can be done by using a decorator, just like this:
```python
@bot.command()
async def mycommand(ctx):
    ...
```
where `bot` is a `commands.Bot` instance. That will create a command that you can call by doing `<prefix>mycommand`
As you can see, the command took the name of the function. You can change that by adding `name="somecoolname"` in the decorator generator. Aliases can also be added by puting `aliases={"onealias", "anotheralias"}`.  

Right now, we have a command that takes no arguments and does nothing. To add functionality to a command you can just write your logic in the decorated function as it will be run when the command is invoked.  

For example, if I want to write a command that is called `hello` to which the bot greets the invoker back, this is what I'd do:
```python
@bot.command(
    name="hello", aliases=["hi", "hullo"], brief="Greet the bot!"
)
async def greet_back_command(ctx):
    await ctx.send(f"Howdy {ctx.author.display_name}!")
```

As you can see there are two new things I used. The `brief` parameter in the command decorator and `ctx` which is the [**Context Arguement**](#the-context-argument).  
The `brief` parameter is purely for documentation purposes and it's rather useful for making short descriptions which look great in help commands.

## The Context Argument

The Context is a very special argument that all commands take. As the name suggests, it provides you with objects relating to the context in which the command was invoked, like the command author, message, channel, guild, etc...
*For the full list of attributes go to the [`commands.Context`](https://discordpy.readthedocs.io/en/latest/ext/commands/api.html#context) on discord.py's docs.*

So, as you saw in the previous code-block I used the `ctx.send` method which is a shortcut for `ctx.channel.send`, in other words, it will send a message to the channel in which the command was invoked in.  

---

Now that you know how to create commands and have played around with the Context argument, you are ready to move on onto the next section - [**Command Arguments**](./command-args.html) - where I'll cover how you can add more arguments to your commands!
